# README #
Cassandra 

### What is this repository for? ###

* Cassandra CRUD

### How do I get set up? ###

* Use docker n/w command to create a network, so that one can easily run multiple    cassandra nodes
* docker network create --driver=bridge --subnet=192.168.60.0/24 --gateway=192.168.60.1  --ip-range=192.168.60.0/24  -o "com.docker.network.bridge.name"="cassandra" cassandra
* Run container
* docker run -p 9042:9042 --name cassandra1 -d cassandra
* docker exec -it cassandra1 /bin/bash
* cqlsh

### For GUI ###
* Datasax DevCenter works fine (https://academy.datastax.com/downloads/ops-center#devCenter)
* https://hub.docker.com/r/abh1nav/opscenter/(Check the link)
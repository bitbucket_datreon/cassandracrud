import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

/**
 * Created by joy on 7/12/17.
 */
public class Crud extends Connection{

    private static final String TABLE_NAME = "sample.products";

    //creating keyspace
    public void createKeyspace(String keyspaceName, String replicationStrategy, int replicationFactor) {
        StringBuilder sb = new StringBuilder("CREATE KEYSPACE IF NOT EXISTS ")
                        .append(keyspaceName).append(" WITH replication = {")
                        .append("'class':'").append(replicationStrategy)
                        .append("','replication_factor':").append(replicationFactor)
                        .append("};");

        String query = sb.toString();
        System.out.println(query);

        session.execute(query);

        System.out.println("Keyspace project created");
    }

    //creating table or column family
    public void createTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(TABLE_NAME).append("(")
                .append("id uuid PRIMARY KEY, ")
                .append("name text,")
                .append("company text);");

        String query = sb.toString();
        session.execute(query);

        System.out.println("Table created");
    }

    //insert
    public void insert() {
        session.execute("INSERT INTO " + TABLE_NAME + " (id, name, company) VALUES (50554d6e-29bb-11e5-b345-feff819cdc9f," +
                "'ssd','Kingston')");
        System.out.println("Values have been inserted");
    }

    //read
    public void read() {

        ResultSet results = session.execute("SELECT * FROM " + TABLE_NAME);
        System.out.println("The values in table are  :");
        for (Row row : results) {
            System.out.format("%s %s %s", "Id : " +row.getUUID("id"),"name : " + row.getString("name"), "company : " +row.getString("company"));
            System.out.println();
        }
    }

    //update
    public void update() {
        session.execute("update " + TABLE_NAME + " set company = 'wd' where id = 50554d6e-29bb-11e5-b345-feff819cdc9f");
        // Select and show the change
        ResultSet results = session.execute("select * from " + TABLE_NAME + " where id = 50554d6e-29bb-11e5-b345-feff819cdc9f");
        for (Row row : results) {
            System.out.format("%s\n", row.getString("company"));
        }
    }

    //delete
    public void deleteKeyspace(String keyspaceName) {
        StringBuilder sb = new StringBuilder("DROP KEYSPACE ").append(keyspaceName);

        String query = sb.toString();
        session.execute(query);
        System.out.println("Keyspace deleted");
    }

}

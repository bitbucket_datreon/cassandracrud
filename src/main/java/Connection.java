import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

/**
 * Created by joy on 7/12/17.
 */
public class Connection {
    private Cluster cluster;

    protected Session session;

    @Override
    public String toString() {
        return "Connection{" +
                "cluster=" + cluster +
                ", session=" + session +
                '}';
    }

    public void connect(String node, Integer port) {
        Cluster.Builder b = Cluster.builder().addContactPoint(node);
        if (port != null) {
            b.withPort(port);

        }
        cluster = b.build();

        session = cluster.connect();

    }

    public Session getSession() {
        return this.session;
    }

    public void close() {
        session.close();
        cluster.close();
    }
}

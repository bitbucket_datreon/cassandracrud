/**
 * Created by joy on 7/12/17.
 */
public class Main {
    public static void main(String[] args) {
        Crud crud = new Crud();
        //Connection connection = new Connection();

        crud.connect("192.168.1.2",9042);
        crud.getSession();

        crud.createKeyspace("sample","SimpleStrategy",1);

        crud.createTable();

        crud.insert();

        crud.read();

        crud.update();

        //crud.deleteKeyspace("sample");

        crud.close();
    }

}
